package main

import (
	"gitlab.com/rtician/vma/src/app"
)

func main() {
	a := app.App{}
	a.Initialize()
	a.Run(":8000")
}

