package tests

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/rtician/vma/src/app"
	"gitlab.com/rtician/vma/src/app/utils"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

var db *gorm.DB

func initApp() *app.App {
	a := app.App{}
	a.Initialize()

	db = utils.Connect()

	return &a
}

func clearTable() {
	initApp()
	db.Exec("DELETE FROM users")
	db.Exec("ALTER TABLE users AUTO_INCREMENT = 1")
}

func makeRequest(method, endpoint string, body io.Reader) *httptest.ResponseRecorder {
	a := initApp()

	req, _ := http.NewRequest(method, endpoint, body)
	req.Header.Set("X-Session-Token", os.Getenv("TOKEN"))

	rr := httptest.NewRecorder()
	a.Router.ServeHTTP(rr, req)

	return rr
}

func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}
