package tests

import (
	"bytes"
	"encoding/json"
	"github.com/Pallinder/go-randomdata"
	"gitlab.com/rtician/vma/src/app/models"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"testing"
)

var users []models.User

func createUsers(count int){
	if count < 1 {
		count = 1
	}

	users = nil
	for i := 0; i < count; i++ {
		u := models.User{
			Name: randomdata.FullName(randomdata.Male),
			Age: randomdata.Number(1, 100),
			Email: randomdata.Email(),
			Address: randomdata.Address(),
			Password: "password",
		}
		models.CreateUser(&u)
		users = append(users, u)
	}
}

func TestUserNotExist(t *testing.T){
	clearTable()

	resp := makeRequest("GET", "/user/1", nil)
	checkResponseCode(t, http.StatusNotFound, resp.Code)

	var m map[string]string
	json.Unmarshal(resp.Body.Bytes(), &m)
	if m["error"] != "User not found" {
		t.Errorf("Expected the 'error' key of the response to be set to 'User not found'. Got '%s'", m["error"])
	}
}

func TestGetUser(t *testing.T) {
	clearTable()
	createUsers(1)

	resp := makeRequest("GET", "/user/1", nil)
	checkResponseCode(t, http.StatusOK, resp.Code)

	var user map[string]interface{}
	json.Unmarshal(resp.Body.Bytes(), &user)
	if user["ID"] == nil {
		t.Errorf("User was not created")
	}
}

func TestCreateUserMissingFields(t *testing.T) {
	clearTable()
	u := models.User{
		Name: randomdata.FullName(randomdata.Male),
		Age: randomdata.Number(1, 100),
		Address: randomdata.Address(),
	}
	jsonData, _ := json.Marshal(u)
	resp := makeRequest("POST", "/user/", bytes.NewBuffer(jsonData))
	checkResponseCode(t, http.StatusOK, resp.Code)

	var m map[string]interface{}
	json.Unmarshal(resp.Body.Bytes(), &m)

	if m["Email"] != "Invalid value" {
		t.Errorf("Email is mandatory")
	}
	if m["Password"] != "Invalid value" {
		t.Errorf("Password is mandatory")
	}
}

func TestCreateUser(t *testing.T) {
	clearTable()
	u := models.User{
		Name: randomdata.FullName(randomdata.Male),
		Age: randomdata.Number(1, 100),
		Email: randomdata.Email(),
		Address: randomdata.Address(),
		Password: "password",
	}
	jsonData, _ := json.Marshal(u)
	resp := makeRequest("POST", "/user/", bytes.NewBuffer(jsonData))
	checkResponseCode(t, http.StatusCreated, resp.Code)

	var m map[string]interface{}
	json.Unmarshal(resp.Body.Bytes(), &m)

	if m["ID"] != 1.0 {
		t.Errorf("Expected product ID to be '1'. Got '%v'", m["id"])
	}
	if m["name"] != u.Name {
		t.Errorf("Expected user name to be '%v'. Got '%v'", u.Name, m["name"])
	}
	if m["age"].(float64) != float64(u.Age) {
		t.Errorf("Expected user age to be '%v'. Got '%v'", u.Age, m["age"])
	}
	if m["email"] != u.Email {
		t.Errorf("Expected user email to be '%v'. Got '%v'", u.Email, m["email"])
	}
	if m["address"] != u.Address {
		t.Errorf("Expected user address to be '%v'. Got '%v'", u.Address, m["address"])
	}
	err := bcrypt.CompareHashAndPassword([]byte(m["password"].(string)), []byte(u.Password))
	if err != nil {
		t.Errorf("Invalid password hashing for user")
	}
}

func TestUpdateUser(t *testing.T) {
	clearTable()
	createUsers(1)

	resp := makeRequest("GET", "/user/1", nil)
	var originalUser map[string]interface{}
	json.Unmarshal(resp.Body.Bytes(), &originalUser)

	u := models.User{
		Name: randomdata.FullName(randomdata.Male),
		Age: randomdata.Number(1, 100),
		Email: randomdata.Email(),
		Address: randomdata.Address(),
		Password: "new password",
	}
	jsonData, _ := json.Marshal(u)
	resp = makeRequest("PUT", "/user/1", bytes.NewBuffer(jsonData))
	checkResponseCode(t, http.StatusOK, resp.Code)

	var m map[string]interface{}
	json.Unmarshal(resp.Body.Bytes(), &m)

	if m["id"] != originalUser["id"] {
		t.Errorf("Expected the id to remain the same (%v). Got %v", originalUser["id"], m["id"])
	}
	if m["name"] == originalUser["name"] {
		t.Errorf("Expected the name to change from '%v' to '%v'. Got '%v'", originalUser["name"], m["name"], m["name"])
	}
	if m["age"] == originalUser["age"] {
		t.Errorf("Expected the age to change from '%v' to '%v'. Got '%v'", originalUser["age"], m["age"], m["age"])
	}
	if m["email"] == originalUser["email"] {
		t.Errorf("Expected user email to be '%v'. Got '%v'", originalUser["email"], m["email"])
	}
	if m["address"] == originalUser["address"] {
		t.Errorf("Expected user address to be '%v'. Got '%v'", originalUser["address"], m["address"])
	}
}

func TestDeleteUser(t *testing.T) {
	clearTable()
	createUsers(1)

	resp := makeRequest("GET", "/user/1", nil)
	checkResponseCode(t, http.StatusOK, resp.Code)

	resp = makeRequest("DELETE", "/user/1", nil)
	checkResponseCode(t, http.StatusOK, resp.Code)

	resp = makeRequest("GET", "/user/1", nil)
	checkResponseCode(t, http.StatusNotFound, resp.Code)
}


