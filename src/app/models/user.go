package models

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/rtician/vma/src/app/utils"
	"golang.org/x/crypto/bcrypt"
)

var db *gorm.DB

type User struct {
	gorm.Model
	Address  string `json:"address"`
	Age      int    `json:"age" validate:"gt=0,lte=160"`
	Email    string `json:"email" validate:"required,email"`
	Name     string `json:"name" validate:"required"`
	Password string `json:"password" validate:"required"`
}

func init() {
	db = utils.Connect()
	db.AutoMigrate(&User{})
}

func (u *User) SetPassword(passwd string) {
	pwd, err := bcrypt.GenerateFromPassword([]byte(passwd), bcrypt.DefaultCost)
	if err != nil {
		panic(err)
	}
	u.Password = string(pwd)
}

func CreateUser(u *User) *User {
	db.NewRecord(u)
	db.Create(&u)
	return u
}

func GetAllUsers() []User {
	var users []User
	db.Find(&users)
	return users
}

func GetUserById(Id int64) (*User, *gorm.DB) {
	var user User
	db.Where("ID = ?", Id).Find(&user)
	return &user, db
}

func DeleteUser(Id int64) User {
	var user User
	db.Where("ID = ?", Id).Delete(user)
	return user
}
