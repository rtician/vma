package controllers

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/rtician/vma/src/app/models"
	"gitlab.com/rtician/vma/src/app/utils"
	"gitlab.com/rtician/vma/src/app/validators"
	"net/http"
	"strconv"
)

func GetUsers(w http.ResponseWriter, r *http.Request) {
	users := models.GetAllUsers()
	utils.JsonResponse(w, http.StatusOK, users)
}

func CreateUser(w http.ResponseWriter, r *http.Request) {
	user := &models.User{}
	json.NewDecoder(r.Body).Decode(user)

	invalidFields, isInvalid := validators.ValidateUser(user)
	if isInvalid == true {
		utils.JsonResponse(w, http.StatusOK, invalidFields)
	} else {
		user.SetPassword(user.Password)
		user = models.CreateUser(user)
		utils.JsonResponse(w, http.StatusCreated, user)
	}
}

func GetUserById(w http.ResponseWriter, r *http.Request){
	vars := mux.Vars(r)
	id, _ := strconv.ParseInt(vars["id"], 0, 0)
	user, _ := models.GetUserById(id)
	if user.ID == 0 {
		utils.ErrorResponse(w, http.StatusNotFound, "User not found")
	} else {
		utils.JsonResponse(w, http.StatusOK, user)
	}
}

func UpdateUser(w http.ResponseWriter, r *http.Request){
	user := &models.User{}
	utils.ParseBody(r, user)

	vars := mux.Vars(r)
	id, _ := strconv.ParseInt(vars["id"], 0, 0)
	currentUser, db := models.GetUserById(id)
	if currentUser.ID == 0 {
		utils.ErrorResponse(w, http.StatusNotFound, "User not found")
		return
	}

	currentUser = user
	currentUser.SetPassword(user.Password)

	invalidFields, isInvalid := validators.ValidateUser(currentUser)
	if isInvalid == true {
		utils.JsonResponse(w, http.StatusOK, invalidFields)
	} else {
		db.Save(&currentUser)
		utils.JsonResponse(w, http.StatusOK, currentUser)
	}
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, _ := strconv.ParseInt(vars["id"], 0, 0)
	user := models.DeleteUser(id)
	utils.JsonResponse(w, http.StatusOK, user)
}