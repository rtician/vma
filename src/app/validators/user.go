package validators

import (
	"github.com/go-playground/validator"
	"gitlab.com/rtician/vma/src/app/models"
)

var validate *validator.Validate

func ValidateUser(u *models.User) (map[string]string, bool) {
	invalidFields := make(map[string]string)
	isInvalid := false

	validate = validator.New()
	err := validate.Struct(u)
	if err != nil {
		isInvalid = true
		if _, ok := err.(*validator.InvalidValidationError); ok {
			panic(err)
		}

		for _, err := range err.(validator.ValidationErrors) {
			invalidFields[err.StructField()] = "Invalid value"
		}
	}
	return invalidFields, isInvalid
}
