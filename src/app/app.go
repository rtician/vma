package app

import (
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"github.com/urfave/negroni"
	"gitlab.com/rtician/vma/src/app/controllers"
	"gitlab.com/rtician/vma/src/app/utils"
	"log"
	"net/http"
)

type App struct {
	Router *mux.Router
	DB *gorm.DB
}

func (a *App) registerUserRoutes() {
	a.Router.HandleFunc("/user/", controllers.CreateUser).Methods("POST")
	a.Router.HandleFunc("/user/", controllers.GetUsers).Methods("GET")
	a.Router.HandleFunc("/user/{id:[0-9]+}", controllers.GetUserById).Methods("GET")
	a.Router.HandleFunc("/user/{id:[0-9]+}", controllers.UpdateUser).Methods("PUT")
	a.Router.HandleFunc("/user/{id:[0-9]+}", controllers.DeleteUser).Methods("DELETE")
}

func (a *App) Initialize() {
	a.Router = mux.NewRouter()
	a.registerUserRoutes()

	amw := utils.InitAuthMiddleware()
	a.Router.Use(amw.Middleware)

	n := negroni.New(negroni.NewRecovery(), negroni.NewLogger())
	n.UseHandler(a.Router)
}

func (a *App) Run(addr string) {
	log.Fatal(http.ListenAndServe(addr, a.Router))
}

