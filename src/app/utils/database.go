package utils

import (
	"github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"os"
)

func Connect() *gorm.DB {
	cfg := &mysql.Config{
		Net:                  "tcp",
		Addr:                 os.Getenv("DB_HOST"),
		DBName:               os.Getenv("DB_NAME"),
		User:                 os.Getenv("DB_USR"),
		Passwd:               os.Getenv("DB_PWD"),
		AllowNativePasswords: true,
		ParseTime:            true,
	}
	db, err := gorm.Open("mysql", cfg.FormatDSN())
	if err != nil {
		panic(err)
	}

	return db
}
