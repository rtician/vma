package utils

import (
	"net/http"
	"os"
)

type authMiddleware struct {
	token map[string]string
}

func InitAuthMiddleware() *authMiddleware {
	amw := &authMiddleware{
		token: make(map[string]string),
	}
	amw.token[os.Getenv("TOKEN")] = "testUser"

	return amw
}

func (amw *authMiddleware) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("X-Session-Token")

		if _, found := amw.token[token]; found {
			next.ServeHTTP(w, r)
		} else {
			http.Error(w, "Forbidden", http.StatusForbidden)
		}
	})
}
