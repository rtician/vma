module gitlab.com/rtician/vma

go 1.15

require (
	github.com/Pallinder/go-randomdata v1.2.0
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.16
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/urfave/negroni v1.0.0
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897
)
