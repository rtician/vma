# VerifyMyAge Backend test

### How to run
```
$ git clone https://gitlab.com/rtician/vma
$ cd vma/docker
$ docker-compose up -d
```

The database and authentication settings are inside docker-compose file

### How to run the tests

```
$ docker-compose up -d
$ docker exec -it vma-app go test ./src/tests/... -v
```
